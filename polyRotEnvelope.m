## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{pR} =} triRotEnvelope (@var{T}, @var{theta})
##
## @end deftypefn

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function pR = triRotEnvelope (T, theta)
  # TODO Find leading and trailing vertices
  # Assuming CCW and rotating around T(3,:)
  v0     = T(3,:).';

  v1     = T(1,:).';
  nv1_sq = sumsq (v1);

  v2     = T(2,:).';
  nv2_sq = sumsq (v2);

  e2     = (v2 - v1); # edge vector
  ne2_sq = sumsq (e2);

  # Cosine of angle between v1 and v2
  ca = (v1 * e2) / sqrt (nv1_sq * ne2_sq);

  # The resulting circular-arc polygon always has the edge from v0 to v1
  pR = {[v0 v1].'};
  if nv2_sq >= nv1_sq

  else
    pR{2} = c1 (theta);
  endif

endfunction
