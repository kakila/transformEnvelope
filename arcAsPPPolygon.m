## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {} {@var{} =} arcAsPPPolygon (@var{}, @var{})
##
## @seealso{}
## @end deftypefn

function ppp = arcAsPPPolygon (arc, n = 5)
  if n < 1 % bound on error term
    # TODO
  else % degree of taylor polynomial
    N      = 0:n;
    ppp    = cell(2,1);

    ppp{1}(1,2:2:2*n+2) = fliplr ( (-1).^N .* (pi).^(2*N) ./ factorial (2*N));
    ppp{1}(2,1:2:2*n+1) = fliplr ( (-1).^N .* (pi).^(2*N+1) ./ factorial (2*N+1));

    ppp{2}(1,:) = -ppp{1}(1,:);
    ppp{2}(2,:) = -ppp{1}(2,:);
  endif
endfunction
